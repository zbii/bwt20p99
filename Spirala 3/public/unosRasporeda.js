function ucitajTabeluPredmeta(data) {    
    let imena = data.split('\n');    
    var tabelaPredmeta = document.getElementById("tijeloTabelePredmeti");
    document.getElementById('tijeloTabelePredmeti').innerHTML = "";
    imena.forEach(element => {
        var red = tabelaPredmeta.insertRow(-1);
        var td = red.insertCell(-1);
        td.innerHTML = element;
    });
}

function ucitajTabeluRaspored(data, err) {
   
    var tabela = document.getElementById("tijeloTabeleRaspored");
    document.getElementById('tijeloTabeleRaspored').innerHTML = "";
       
    if (err) 
    {
            var red = tabela.insertRow(-1);
            var td1 = red.insertCell(-1);
            var td2 = red.insertCell(-1);
            var td3 = red.insertCell(-1);
            var td4 = red.insertCell(-1);
            var td5 = red.insertCell(-1);
            td1.innerHTML = "";
            td2.innerHTML = "";
            td3.innerHTML = "";
            td4.innerHTML = "";
            td5.innerHTML = "";
    }
    else
    {
        for (var i = 0; i < data.length; i++) 
        {
            var red = tabela.insertRow(-1);
            var td1 = red.insertCell(-1);
            var td2 = red.insertCell(-1);
            var td3 = red.insertCell(-1);
            var td4 = red.insertCell(-1);
            var td5 = red.insertCell(-1);
            td1.innerHTML = data[i].naziv;
            td2.innerHTML = data[i].aktivnost;
            td3.innerHTML = data[i].dan;
            td4.innerHTML = data[i].pocetak;
            td5.innerHTML = data[i].kraj;
        }    
    }        
}


ucitajSortirano(null,null,ucitajTabeluRaspored);

//app.get("/predmeti"
function ucitajPredmete() {
    $.ajax({
        method: 'GET',
        url: 'http://localhost:8080/predmeti',
        success: (data) => ucitajTabeluPredmeta(data)
    });
}


//app.delete("/predmeti"
function obrisiPredmet (ime) {
    $.ajax({
        method: 'DELETE',
        url: 'http://localhost:8080/predmeti?ime=' + ime
    });
}

//app.post("/raspored" 
function dodajNovuAKtivnost(ime) {
    var link = 'http://localhost:8080/raspored?naziv=' + document.getElementById('naziv').value + '&aktivnost=' +
        document.getElementById('aktivnost').value +
        '&dan=' + document.getElementById('dan').value + '&startHH=' +
        document.getElementById('startHH').value + '&startMM=' + document.getElementById('startMM').value + '&endHH=' +
        document.getElementById('endHH').value + '&endMM=' + document.getElementById('endMM').value;
        
        $.ajax({
        method: 'POST',
        url: link,
        success: () => {
            alert("DESILO SE 11");        
            ucitajPredmete();
            ucitajSortirano(null, null, ucitajTabeluRaspored);    
            alert("DESILO SE 12");        
        },
        error: () => {
            alert("DESILO SE 21");
        obrisiPredmet(ime); 
        alert("DESILO SE 22");      }
    });
}

//app.post("/predmeti"
function dodajPredmet () {    
    $.ajax({
        method: 'POST',
        url: 'http://localhost:8080/predmeti?predmet=' + document.getElementById('naziv').value,
        success: () => {            
            dodajNovuAKtivnost(document.getElementById('naziv').value);}
    });
}

function dodajAktivnost() {
    
    dodajPredmet();
}

ucitajPredmete();