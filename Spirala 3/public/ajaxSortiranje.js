function ucitajSortirano(dan,atribut,callback) {

   var putanja = "http://localhost:8080/raspored";

   //Ovdje generišemo ostatak URL-a u odnosu na vrijednosti parametara dan i atribut
    if (atribut != null && atribut.length > 0) 
      putanja = putanja + "?sort=" + atribut;

    if (dan != null && dan.length > 0){
        if (atribut != null && atribut.length > 0) 
          putanja = putanja + "&";
        else 
          putanja = putanja + "?";
        putanja = putanja + "dan=" + dan;
    }

    $.ajax({
      type: "GET",
      url: putanja,
      success: (data) => callback(data, null),
      error: (err) => callback(null, err),
 }); 
}