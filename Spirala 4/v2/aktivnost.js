const Sequelize = require("sequelize");

module.exports = function (sequelize, DataTypes) {
    const aktivnost = sequelize.define("aktivnost", {
    	naziv: Sequelize.STRING,
    	pocetak: Sequelize.FLOAT,
    	kraj: Sequelize.FLOAT
    });
    return aktivnost;
};