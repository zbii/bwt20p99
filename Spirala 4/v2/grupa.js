const Sequelize = require("sequelize");

module.exports = function (sequelize, DataTypes) {
    const grupa = sequelize.define("grupa", {
    	naziv: Sequelize.STRING
    });
    return grupa;
};