const Sequelize = require("sequelize");

module.exports = function (sequelize, DataTypes) {
    const tip = sequelize.define("tip", {
    	naziv: Sequelize.STRING,
    });
    return tip;
};