$(document).ready(function() {

    //učitavanje vrijednosti iz textarea
        $("#kraj").on("click", function() {
            let redovi = document.getElementById("unos").value.split("\n");
            let uneseniStudenti = [];
            redovi.forEach(linija => {

                if(linija != "") {
                    let student1 = linija.split(",");  //odvajamo studenta i br. indeksa
                    let student = {
                        ime: student1[0].trim(),
                        index: student1[1].trim()
                    };
                    //unosimo studenta
                    uneseniStudenti.push(student);
                }
                $.ajax({
                    url: "/v2/student",
                    method: "POST",
                    data: JSON.stringify(uneseniStudenti),
                    success: function(data) { return data; }, 
                    error: function(err) { throw err; }
                })
            })
        })
    
        //Uzimamo uneseno
        var grupa = document.getElementById("grupa");
        $.ajax({
            url: "/v2/grupa",
            method: "GET",
            success: (data) => {
                data.forEach(student => { //svakog studenta stavljamo u unesenu grupu
                    let tekst = document.createElement("grupa");
                    tekst.text = student.ime;
                    grupa.add(tekst);
                })
            }
        })    
    });