const Sequelize = require('sequelize');
const sequelize = new Sequelize("wt2099ST","root","",{host:"127.0.0.1",dialect:"mysql"});

sequelize.authenticate().then(() => {
	console.log('Uspjesna konekcija');
}).catch(err => {
   console.error('Neuspjesna konekcija, error:', err);
});

const db={};

db.Sequelize = Sequelize;  
db.sequelize = sequelize;


//import modela
db.predmet = sequelize.import(__dirname+'/predmet.js');
db.student = sequelize.import(__dirname+'/student.js');
db.grupa = sequelize.import(__dirname+'/grupa.js');
db.aktivnost = sequelize.import(__dirname+'/aktivnost.js');
db.dan = sequelize.import(__dirname+'/dan.js');
db.tip = sequelize.import(__dirname+'/tip.js');

//relacije

//relacije 1-N
//jedan predmet može imati više grupa
db.predmet.hasMany(db.grupa,{as:"grupe"});
db.grupa.belongsTo(db.predmet,{as:"grupe"});

//relacije N-1
//jedan predmet može imati više aktivnosti
db.predmet.hasMany(db.aktivnost,{as:"prAktivnosti"});
db.aktivnost.belongsTo(db.predmet,{as:"prAktivnosti"});

//u jednom danu može biti više aktivnosti
db.dan.hasMany(db.aktivnost,{as:"dnAktivnosti"});
db.aktivnost.belongsTo(db.dan,{as:"dnAktivnosti"});

//svaki tip može imati više aktivnosti
db.tip.hasMany(db.aktivnost,{as:"tipAktivnosti"});
db.aktivnost.belongsTo(db.tip,{as:"tipAktivnosti"});

//relacije N-0
db.grupa.hasMany(db.aktivnost,{as:"grAktivnost", allowNull:true});
db.aktivnost.belongsTo(db.grupa,{as:"grAktivnost", allowNull:true});

//relacije N-M
db.studentGrupa = db.student.belongsToMany(db.grupa,{as:"studenti", through:"studentGrupa", foreignKey:"studentID"});
db.grupa.belongsToMany(db.student,{as:"studenti", through:"studentGrupa", foreignKey:"grupeID"});

module.exports = db;