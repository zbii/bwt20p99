const Sequelize = require("sequelize");

module.exports = function (sequelize, DataTypes) {
    const student = sequelize.define("student", {
    	ime: Sequelize.STRING,
    	index: Sequelize.STRING,
    });
    return student;
};