var http = require('http');
var fs = require('fs');
var url = require('url');
var express = require('express');
const session = require("express-session");
var path = require('path');
const ajax = require("./v1/public/ajaxSortiranje.js");
var app = express();
const db = require('./v2/db.js');
var bodyParser = require('body-parser');

app.use(express.static(path.join(__dirname, 'v1/public')));
app.use(express.static(path.join(__dirname, 'v2')));

app.get('public/naziv', function (req, res) {
    res.sendFile('public/naziv', {
        root: __dirname
    });
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

/*****************U NASTAVKU SE NALAZE POMOĆNE FUNKCIJE KOJE SU IMPLEMENTIRANE U PRVOJ PARCIJALI***************** */
//Funkcija parsiranja dana u broj
function danUBroj(dan) {
    let daniUSedmici = ['ponedjeljak', 'utorak', 'srijeda', 'cetvrtak', 'petak', 'subota', 'nedjelja'];   
    let broj = 0;
    for(var i = 0; i < daniUSedmici.length; i++) 
        if(dan == daniUSedmici[i]) 
            broj = i + 1;
    if(dan == 'četvrtak') 
        return 4;
    else return broj;
}

//Provjera da li je dan ispravno unesen
function validirajDan (element)
{
    return (element.toLowerCase() == 'ponedjeljak' || element.toLowerCase() == 'utorak' || element.toLowerCase() == 'srijeda' || element.toLowerCase() == 'četvrtak' || element.toLowerCase() == 'cetvrtak' || element.toLowerCase() == 'petak' || element.toLowerCase() == 'subota' || element.toLowerCase() == 'nedjelja');
}

//Pomoćna funkcija za pretvaranje vremena u minute zbog lakšeg poređenja
function dajUMinutama(vrijeme)
{    
    return (parseInt(vrijeme[3]*10) + parseInt(vrijeme[4]) + (parseInt(vrijeme[0]*10) + parseInt(vrijeme[1]))*60);
}

function sortirajRaspored (rasporedGrupe)
{
    //Pomoćna funkcija za sortiranje rasporeda - funkcija kriterija nam prvo sortira po danima a onda po vremenu
    rasporedGrupe.sort((red1, red2) => {
        if(danUBroj(red1.dan) > danUBroj(red2.dan)) return 1;
        else if(danUBroj(red1.dan) < danUBroj(red2.dan)) return -1;
        else {
            if(dajUMinutama(red1.pocetak) > dajUMinutama(red2.pocetak)) 
                return 1;
            else if(dajUMinutama(red1.pocetak) < dajUMinutama(red2.pocetak)) 
                return -1;
            return 0;
            }
    });
 return rasporedGrupe;    
}

function dajAtributKaoBroj (atr)
{
    if (atr == 'naziv')
        return 0;
    else if (atr == 'aktivnost')
        return 1;
    else if (atr == 'dan')
        return 2;
    else if (atr == 'pocetak')
        return 3;
    else if (atr == 'kraj')
        return 4;
    else return -1;
}

function sortirajPoAtributu (raspored, atrTemp)
{
    //Najprije provjeravamo validnost prvog slova u query objectu za parametar sort, ako nije A ili D vraćamo nesortirani raspored
    if (atrTemp[0].toUpperCase() != 'A' && atrTemp[0].toUpperCase() != 'D')   
    {           
        return raspored;
    }
    //Sada izuzimamo drugi dio parametra u query sort objektu i onda pozivamo funkciju koja nam vraća redni broj 
    //parametra po kojem trebamo sortirati kako bismo izbjegli stalno upoređivanje, pozivamo samo po tom indeksu
    var atribut = atrTemp.substr(1);
    var index = dajAtributKaoBroj(atribut);

    if (index == -1)
        return raspored;
    
    //Ako je parametar za sortiranje dan u sedmici, umjesto sortiranja po abecednom poretku intuitivnije je sortiranje
    //po broju dana, pa taj slučaj posebno određujemo 

    if (index == 2)
    {
        raspored.sort((red1, red2) => {
            var red1Temp = red1.split(',');
            var red2Temp = red2.split(',');
    
            if(danUBroj(red1Temp[index]) > danUBroj(red2Temp[index])) return 1;
            else if(danUBroj(red1Temp[index]) < danUBroj(red2Temp[index])) return -1;   
            else return 0;     
        });
    }
    else if (index == 3 || index == 4) //DOdatak na prethodnu spiralu, sortiranje po vremenu početka
    {
        raspored.sort((red1, red2) => {
            var red1Temp = red1.split(',');
            var red2Temp = red2.split(',');
            if(dajUMinutama(red1Temp[index]) > dajUMinutama(red2Temp[index])) return 1;
            else if(dajUMinutama(red1Temp[index]) < dajUMinutama(red2Temp[index])) return -1;   
            else return 0;     
        });
    }
    else {
        //Sortiramo raspored u rastući poredak po parametru
        raspored.sort((red1, red2) => {
            var red1Temp = red1.split(',');
            var red2Temp = red2.split(',');

            if(red1Temp[index] > red2Temp[index]) return 1;
            else if(red1Temp[index] < red2Temp[index]) return -1;   
            else return 0;     
        });
    }

    //Ako je prvo slovo D, obrćemo sortirani raspored u opadajući poredak
    if (atrTemp[0].toUpperCase() == 'D')
        raspored.reverse();

    return raspored;    
}

function izdvojiRaspored(raspored, imeGrupe) {
    let rasporedGrupe = [];
    //U ovoj petlji uzimamo sve redove rasporeda koji imaju datu grupu, a uključujemo i predavanja jer grupe to zajedno slušaju
    for(let i = 0; i < raspored.length; i++) 
        if(raspored[i].naziv.includes(imeGrupe) || raspored[i].aktivnost.includes('predavanj')) 
           rasporedGrupe.push(raspored[i]);
    return sortirajRaspored(rasporedGrupe);
}
/********************************************************------------------------- */

function provjeriImeP(element) {
    return element.match(/^[A-Z]+[0-9]?$/);
}

function provjeriImeV(element) {
    return element.match(/^[A-Z]+ ?[0-9]?-grupa ?[0-9]+$/);
}

function dajUMinutama(vrijeme) {
    return (parseInt(vrijeme[3] * 10) + parseInt(vrijeme[4]) + (parseInt(vrijeme[0] * 10) + parseInt(vrijeme[1])) * 60);
}

function uporediVremena(vrijeme1, vrijeme2) {
    return (dajUMinutama(vrijeme1) < dajUMinutama(vrijeme2));
}

//Regex funkcija koja provjerava validnost vrijednosti vremena
function provjeriVrijeme (element)
{
    return element.match(/^([0-1]?[0-9]|2[0-3]):[0-5][0-9]$/);
}

//Provjera formata vremena - pozivamo i pomoćnu funkciju koja provjerava validnost vrijednosti vremena
function validirajVrijeme(vrijeme) {
    return !(vrijeme == "" || vrijeme.length != 5 || provjeriVrijeme(vrijeme) == null);
}

function validirajZahtjev(ime1 = '', aktivnost1 = '', dan1 = '', start1 = '', end1 = '') {
    
    if(ime1 == null || aktivnost1 == null || dan1 == null || start1 == null || end1==null)
    {
        console.log("Neispravni podaci!");
        return false;
    }

    //Validacija da li su ikako uneseni podaci zahtjeva
    if (ime1.length == 0) {
        console.log("Nepostojece ime predmeta");
        return false;
    }
    if (aktivnost1.length == 0) {
        console.log("Nepostojece ime aktivnosti");
        return false;
    }
    if (dan1.length == 0) {
        console.log("Nepostojece ime dana");
        return false;
    }
    if (start1.length == 0)
    {
        console.log("Nepostojeće vrijeme početka aktivnosti");
        return false;
    }
    if (end1.length == 0)
    {
        console.log("Nepostojeće vrijeme kraja aktivnosti");
        return false;
    }

    //Validacija ispravnosti unesenih podataka
    if (aktivnost1 == "predavanje") {
        if (provjeriImeP(ime1) == null) {
            console.log("Molimo unesite validno ime predmeta");
            return false;
        }
    } else if (aktivnost1 == "vjezbe" || aktivnost1 == "vježbe") {
        if (provjeriImeV(ime1) == null) {
            console.log("Molimo unesite validno ime predmeta i grupu u formatu predmet-grupa");
            return false;
        }
    }
    else if (aktivnost1 != "predavanje" && aktivnost1 != "vjezbe" || aktivnost1 != "vježbe")
    {
        console.log("Molimo unesite validnu aktivnost");
        return false;
    }

    /*Validacija dana i vremena*/
    //Validacija unesenog vremena se provjerava korištenjem regex izraza na format HH:MM
    //Na kraju se provjerava da li je vrijeme početka aktivnosti prije vremena kraja aktivnosti
    if (validirajDan(dan1) == false) {
        console.log("Neispravan dan odvijanja aktivnosti!");
        return false;
    }

    if (validirajVrijeme(start1) == false) {
        console.log("Neispravno početno vrijeme!");
        return false;
    }
    if (validirajVrijeme(end1) == false) {
        console.log("Neispravno krajnje vrijeme!");
        return false;
    }

    if (uporediVremena(start1, end1) == false) {
        console.log("Vrijeme početka vježbe mora biti prije vremena kraja!");
        return false;
    }

    return true;
}

function pretvoriUJSON (rasporedString)
{
    var raspored = [];
    //Smještamo svaki red u poseban element niza redovi
    let redovi = rasporedString.split('\n');
    //U ovoj rasponskoj petlji uzimamo svaki od redova i dijelimo ga na individualne elemente kako je navedeno u cvs formatu
    //Zatim pozivom funkcije validirajRed kojoj šaljemo svaki od redova određujemo da li je red u ispravnom formatu
    //Ako jeste, ubacujemo ga u raspored, a ako nije preskačemo ga
    redovi.forEach(red => {
        red = red.trim();        
        let elementiReda = red.split(','); 
        if (elementiReda.length == 5)               
            raspored.push({naziv: elementiReda[0], aktivnost: elementiReda[1], dan: elementiReda[2], pocetak: elementiReda[3], kraj: elementiReda[4]}); 
        });
    return raspored;
}

function imaLiPreklapanja (start1, end1, start2, end2)
{
    var pocetno1 = dajUMinutama(start1);
    var krajnje1 = dajUMinutama(end1);
    var pocetno2 = dajUMinutama(start2);
    var krajnje2 = dajUMinutama(end2);

    if ((pocetno1 <= pocetno2 && krajnje1 >= krajnje2) || 
        (pocetno1 >= pocetno2 && krajnje1 <= krajnje2) ||
        (pocetno1 <= pocetno2 && krajnje1 <= krajnje2 && pocetno2 <= krajnje1) || 
        (pocetno1 >= pocetno2 && krajnje1 >= krajnje2 && pocetno1 <= krajnje2))
        return true;
    return false;
}


app.get("/predmeti", (req, res) => {
    fs.readFile('public/predmeti.csv', (error, bf) => {
        if (error) {
            res.writeHead(400, {'Content-Type': 'text/csv'});
            res.write('{\"greska\":\"Datoteka predmeti.csv ne postoji!\"}');
            res.end();
            return;
        }

        res.writeHead(200, {'Content-Type': 'text/csv'});
        res.write(bf.toString('utf-8'));
        res.end();
    });
});


app.get('/raspored', (req,res) => { 

    const queryObject = url.parse(req.url,true).query;
            //Ako je queryObject null provjeravamo koji je header Accept
            if (queryObject['dan'] == null)
            {
                //Ako je Accept header text/csv vraćamo sadržaj datoteke
                if(req.headers.accept == 'text/csv')
                {
                    fs.readFile('public/raspored.csv', (error, bf) => {
                        if(error) {
                            res.writeHead(200,{'Content-Type': 'text/csv'});
                            res.write('{\"greska\":\"Datoteka raspored.csv nije kreirana!\"}');
                            res.end();
                            return;
                        } else {
                            res.writeHead(200,{'Content-Type': 'text/csv'});
                            let redovi = bf.toString('utf-8').split('\n');    
                            
                            /*DODATI SORTIRANJE if (queryObject['sort'] != null)*/
                            if (queryObject['sort'] != null)
                                redovi = sortirajPoAtributu(redovi, queryObject['sort'])
    
                            for(var i = 0; i < redovi.length; i++) {
                                var elementiReda = redovi[i].split(',');
                                res.write(elementiReda[0] + ',' + elementiReda[1] + ',' + elementiReda[2] + ',' + elementiReda[3] + ',' + elementiReda[4]);
                                if(i != (redovi.length - 1)) 
                                    res.write('\n');
                            }
                            res.end();
                            return;
                        }
                    });
                }
                //Ako Accept header nije text/csv vraćamo niz JSON objekata
                else 
                {
                    fs.readFile('public/raspored.csv', (error, bf) => {
                        if(error) {
                            res.writeHead(200,{'Content-Type': 'text/json'});
                            res.write('{\"greska\":\"Datoteka raspored.csv nije kreirana!\"}');
                            res.end();
                            return;
                        } else {
                            res.writeHead(200,{'Content-Type': 'text/json'});
                            let redovi = bf.toString('utf-8').split('\n');     
                            
                            /*DODATI SORTIRANJE if (queryObject['sort'] != null)*/
                            if (queryObject['sort'] != null)
                                redovi = sortirajPoAtributu(redovi, queryObject['sort'])
    
                            res.write('[');
                            for(var i = 0; i < redovi.length; i++) {
                                var elementiReda = redovi[i].split(',');
                                res.write('{\"naziv\":\"' + elementiReda[0] + '\",\"aktivnost\":\"' + elementiReda[1] + '\",\"dan\":\"' 
                                    + elementiReda[2] + '\",\"pocetak\":\"' + elementiReda[3] + '\",\"kraj\":\"' + elementiReda[4] + '\"}');
                                if(i != (redovi.length - 1)) 
                                    res.write(',');
                            }
                            res.write(']');
                            res.end();
                            return;
                        }
                    });
                }
            }
            else
            {
                //Provjeravamo da li je dan ispravan
    
                //Ako dan nije ispravan a Accept header je text/csv vraćamo prazan tekst
                if (validirajDan(queryObject['dan']) == false && req.headers.accept == 'text/csv')
                {
                    res.writeHead(200,{'Content-Type': 'text/csv'});
                    res.write('');
                    res.end();
                    return;
                }
                //Ako dan nije ispravan a Accept header nije text/csv vraćamo prazan JSON niz
                else if (validirajDan(queryObject['dan']) == false)
                {
                    res.writeHead(200,{'Content-Type': 'text/json'});
                    res.write('[]');
                    res.end();
                    return;
                }
                
                //Ako smo prošli obje prethodne provjere, provjeravamo da li je Accept header text/csv i ako
                //Jeste vraćamo sadržaj datoteke, ali samo one redove kod koji parametar dan ima vrijednost
                //iz query objekta
                if(req.headers.accept == 'text/csv')
                {
                    fs.readFile('public/raspored.csv', (error, bf) => {
                        if(error) {
                            res.writeHead(200,{'Content-Type': 'text/csv'});
                            res.write('{\"greska\":\"Datoteka raspored.csv nije kreirana!\"}');
                            res.end();
                            return;
                        } else {
                            res.writeHead(200,{'Content-Type': 'text/csv'});
                            let redovi = bf.toString('utf-8').split('\n');  
                            /*DODATI SORTIRANJE if (queryObject['sort'] != null)*/
                            if (queryObject['sort'] != null)
                            {
                                redovi = sortirajPoAtributu(redovi, queryObject['sort'])
                            }
                            
                            for(var i = 0; i < redovi.length; i++) {
                                var elementiReda = redovi[i].split(',');                            
                                if (elementiReda[2].toUpperCase() == queryObject['dan'].toUpperCase() || 
                                (elementiReda[2].toUpperCase() == "ČETVRTAK" && queryObject['dan'].toUpperCase() == "CETVRTAK") ||
                                (elementiReda[2].toUpperCase() == "CETVRTAK" && queryObject['dan'].toUpperCase() == "ČETVRTAK"))                            
                                {
                                    res.write(elementiReda[0] + ',' + elementiReda[1] + ',' + elementiReda[2] + ',' + elementiReda[3] + ',' + elementiReda[4]);
                                    if(i != (redovi.length - 1)) 
                                        res.write('\n');
                                }
                            }
                            res.end();
                            return;
                        }
                    });
                }            
                //Ako accept header nije text/csv, vraćamo niz JSON objekata, onih kojima je parametar dan
                //Isti kao vrijednost queryObject-a
                else
                {
                    fs.readFile('public/raspored.csv', (error, bf) => {
                        if(error) {
                            res.writeHead(200,{'Content-Type': 'text/json'});
                            res.write('{\"greska\":\"Datoteka raspored.csv nije kreirana!\"}');
                            res.end();
                            return;
                        } else {
                            res.writeHead(200,{'Content-Type': 'text/json'});
                            
                            let redoviTemp1 = bf.toString('utf-8').split('\n');      
                            let redovi = [];
    
                            for (let i = 0; i < redoviTemp1.length; i++)
                            {
                                let tempRed = redoviTemp1[i].split(',');
                                if ((tempRed[2].toLowerCase() == queryObject['dan'].toLowerCase()) || 
                                (tempRed[2].toUpperCase() == "ČETVRTAK" && queryObject['dan'].toUpperCase() == "CETVRTAK"))
                                    redovi.push(redoviTemp1[i]);
                            }
                            
                            if (queryObject['sort'] != null)
                            {
                                redovi = sortirajPoAtributu(redovi, queryObject['sort']);
                            }
    
                            res.write('[');
                            for(var i = 0; i < redovi.length; i++) {
                                var elementiReda = redovi[i].split(',');
                                if (elementiReda[2].toUpperCase() == queryObject['dan'].toUpperCase() || 
                                (elementiReda[2].toUpperCase() == "ČETVRTAK" && queryObject['dan'].toUpperCase() == "CETVRTAK") ||
                                (elementiReda[2].toUpperCase() == "CETVRTAK" && queryObject['dan'].toUpperCase() == "ČETVRTAK"))   
                                {
                                    res.write('{\"naziv\":\"' + elementiReda[0] + '\",\"aktivnost\":\"' + elementiReda[1] + '\",\"dan\":\"' 
                                        + elementiReda[2] + '\",\"pocetak\":\"' + elementiReda[3] + '\",\"kraj\":\"' + elementiReda[4] + '\"}');
                                    if(i != (redovi.length - 1)) 
                                        res.write(',');
                                }
                            }
                            res.write(']');
                            res.end();
                            return;
                        }
                    });
                }
            }
    
    });


app.delete("/predmeti", (req, res) => {
        fs.readFile('public/predmeti.csv', (error, bf) => {
            if (error) {
                res.writeHead(400, {'Content-Type': 'text/csv'});
                res.write('{\"greska\":\"Datoteka predmeti.csv ne postoji!\"}');
                res.end();
                return;
            }
            var imenaPredmeta = bf.toString('utf-8').split('\n').filter((el) => el != req.query.ime.toString());
            var filtrirana = '';

            for(var i = 0; i < imenaPredmeta.length; i++) 
            {
                if (i == imenaPredmeta.length - 1)
                filtrirana += imenaPredmeta[i];
                else filtrirana += imenaPredmeta[i] + '\n';
            }

            fs.writeFile('public/predmeti.csv', filtrirana, (error) => {
                if (error) {res.writeHead(400, {'Content-Type': 'text/csv'});
                    res.write('{\"greska\":\"Datoteka predmeti.csv ne postoji!\"}');
                    res.end();
                    return;
                }
                
            });
        });
    });

app.post("/predmeti", (req, res) => {

    var imePredmeta = req.query.predmet.toString();

    fs.readFile('public/predmeti.csv', (error, bf) => {

        if (error) {
            res.writeHead(400, {'Content-Type': 'text/csv'});
            res.write('{\"greska\":\"Datoteka predmeti.csv ne postoji!\"}');
            res.end();
            return;
        }

        var imena = bf.toString('utf-8').split('\n');
        for (var i = 0; i < imena.length; i++) {
            if (imena[i] == imePredmeta) {
                res.writeHead(200, {});
                res.end();
                return;
            }
        }

        var ime = "\n" + imePredmeta;
        fs.appendFile('public/predmeti.csv', ime, function (error) {
            if (error) throw error;
            res.write('Uspjesno dodavanje novog predmeta');
            res.end();
            return;
        });
    });
});

app.post('/raspored', (req,res) => { 
           
console.log("OVDJE");

            let zahtjev = req.query;


            
        console.log("RADI" + zahtjev); 
            let predmet = zahtjev.naziv;
            let aktivnost = zahtjev.aktivnost;
            let dan = zahtjev.dan;
            let pocetnoVrijemeHH = zahtjev.startHH;
            let pocetnoVrijemeMM = zahtjev.startMM;
            let krajnjeVrijemeHH = zahtjev.endHH;
            let krajnjeVrijemeMM = zahtjev.endMM;

            let pocetnoVrijeme;
            let krajnjeVrijeme;

                //Kreiramo varijable vremena formata HH:MM, u slučaju da nedostaje 0 ispred dodajemo je
                if (pocetnoVrijemeHH.length == 1 && pocetnoVrijemeMM.length == 1)
                    pocetnoVrijeme = '0' + pocetnoVrijemeHH + ':0' + pocetnoVrijemeMM;
                else if (pocetnoVrijemeHH.length == 2 && pocetnoVrijemeMM.length == 1)
                    pocetnoVrijeme = pocetnoVrijemeHH + ':0' + pocetnoVrijemeMM;
                else if (pocetnoVrijemeHH.length == 1 && pocetnoVrijemeMM.length == 2)
                    pocetnoVrijeme = '0' + pocetnoVrijemeHH + ':' + pocetnoVrijemeMM;
                else pocetnoVrijeme = pocetnoVrijemeHH + ':' + pocetnoVrijemeMM;

                if (krajnjeVrijemeHH.length == 1 && krajnjeVrijemeMM.length == 1)
                    krajnjeVrijeme = '0' + krajnjeVrijemeHH + ':0' + krajnjeVrijemeMM;
                else if (krajnjeVrijemeHH.length == 2 && krajnjeVrijemeMM.length == 1)
                    krajnjeVrijeme = krajnjeVrijemeHH + ':0' + krajnjeVrijemeMM;
                else if (krajnjeVrijemeHH.length == 1 && krajnjeVrijemeMM.length == 2)
                    krajnjeVrijeme = '0' + krajnjeVrijemeHH + ':' + krajnjeVrijemeMM;
                else krajnjeVrijeme = krajnjeVrijemeHH + ':' + krajnjeVrijemeMM;
              
                if (validirajZahtjev(predmet, aktivnost, dan, pocetnoVrijeme, krajnjeVrijeme) == false)
                {
                    res.writeHead(400, "Greska u validnosti zahtjeva!");
                    res.end();
                    return;
                }

                //Nakon validiranja aktivnosti možemo kreirati objekat u koji smještamo vrijednosti
                let aktivnostZaDodati = predmet + ',' + aktivnost + ',' + dan + ',' + pocetnoVrijeme + ',' + krajnjeVrijeme; 

                //Sada ucitavamo datoteku raspored.csv u string, a zatim parsiramo u JSON format
                fs.readFile('public/raspored.csv', (error, bf) => {
                    if(error) throw error;
                    let rasporedString = bf.toString('utf-8');
                    let raspored = pretvoriUJSON(rasporedString);

                    //AKo je raspored prazan, ubacujemo novu aktivnost i to je ujedno i jedina aktivnost 
                    //koja se treba naći u fileu raspored.csv, pa ga možemo overwrite-ati
                    if(raspored.length == 0) {
                        fs.appendFile('public/raspored.csv', aktivnostZaDodati, function(error) {
                            if(error) throw error;
                            res.write('Uspjesno ste dodali novu aktivnost');
                            res.end();
                            return;
                        });
                    }                    
                    else 
                    {
                        //U slučaju da raspored.length nije 0, tj. da u datoteci raspored.csv već imamo neke elemente
                        //najprije provjeravamo da nemamo preklapanje nove aktivnosti sa već postojećom, i ako nemamo
                        //tek onda ubacujemo element u datoteku. Za to koristimo neku pomoćnu varijablu
                        let flag = false;

                        //Najprije provjeravamo da li imamo nekih podudaranja sa već postojećim aktivnostima a da je
                        //Pri tome naziv predmeta isti. Jer intuitivno, ako u raspored već imamo unesena BWT predavanja,
                        //ili BWT vježbe za neku određenu grupu, onda se dodavanje još jednih smatra preklapanjem                        
                        raspored.forEach(red => {
                            if (red.naziv.toUpperCase() == predmet.toUpperCase())
                                flag = true;                                
                            });

                        //Sada provjeravamo da li je aktivnost koja se dodaje predavanje, jer u slučaju da jeste
                        //ne smijemo imati podudarnost ni sa jednom drugom aktivnošću unutar cijelog rasporeda
                        //Najprije tražimo sve aktivnosti za taj dan, i onda provjeravamo vrijeme odvijanja aktivnosti
                        //Dodala sam i sliku koje sve slučajeve preklapanja moramo obuhvatiti
                        if (aktivnost.toUpperCase() == 'PREDAVANJE')   
                            raspored.forEach(red => {
                                if ((red.dan.toUpperCase() == dan.toUpperCase() || (red.dan.toUpperCase() == 'ČETVRTAK' && dan.toUpperCase() == 'CETVRTAK') || (red.dan.toUpperCase() == 'CETVRTAK' && dan.toUpperCase() == 'ČETVRTAK')) && imaLiPreklapanja(pocetnoVrijeme, krajnjeVrijeme, red.pocetak, red.kraj) == true)
                                    flag = true;                                                                  
                            });
                        
                        //Ako je aktivnost koja se dodaje vježba za neku određenu grupu, u tom slučaju ne
                        //smijemo imati preklapanje sa predavanjima i sa drugim aktivnostima koje ima ta grupa
                        //pa moramo filtrirati raspored
                        if (aktivnost.toUpperCase() == 'VJEŽBE' || aktivnost.toUpperCase() == 'VJEZBE')
                        {                            
                            let grupa = predmet.split('-')[1];
                            let rasporedGrupe = izdvojiRaspored(raspored, grupa);
                            rasporedGrupe.forEach(red => {
                                if ((red.dan.toUpperCase() == dan.toUpperCase() || (red.dan.toUpperCase() == 'ČETVRTAK' && dan.toUpperCase() == 'CETVRTAK') || (red.dan.toUpperCase() == 'CETVRTAK' && dan.toUpperCase() == 'ČETVRTAK')) && imaLiPreklapanja(pocetnoVrijeme, krajnjeVrijeme, red.pocetak, red.kraj) == true)
                                    flag = true;                                                                  
                            });
                        }
                        
                        //Ako je pomoćna varijabla do kraja provjere ostala false, znači da nema preklapanja i možemo dodati 
                        //novu aktivnost u raspored. Ako je došlo do preklapanja ispisujemo poruku i ne mjenjamo datoteku                    
                        if (flag == false)
                        {
                            var temp = '\n' + aktivnostZaDodati;  
                            fs.appendFile('public/raspored.csv', temp, function(error) {
                                if(error) throw error;                                
                            });
                            res.write('Uspjesno ste dodali novu aktivnost!');                    
                            res.end();
                            return;
                        }                    
                        else {
                            res.writeHead(400, "Dodavanje aktivnosti u datoteku nije uspjelo!");
                            res.end();
                            return;
                        }
                    } 
                 });   
                       
            });

/******************************CRED */



/**********************CREATE */

app.post("/v2/student", (req,res) => {
    let tijeloZahtjeva = JSON.stringify(req.body);
    
    let redovi = tijeloZahtjeva.split("},{");
    
    for (var i = 0; i < redovi.length;i++)
    {
        var elementi = redovi[i].split(",");
        var ime = (elementi[0].split(":")[1]).replace(/\W/g, '');
        var indeks = (elementi[1].split(":")[1]).replace(/\W/g, '');
        var nizPoruka = [];
        db.student.findOne({
            where: { 
                ime: ime
            }
        }).then(() => {
            
            
        });

        db.student.findOne({
            where: {
                indeks: indeks
            }
        }).then(() => {
            nizPoruka.push("Student vec postoji u tabeli sa indexom " + indeks + "\n");
            res.write();
        });

        if (nizPoruka.length != 0)
        {
            res.write(nizPoruka);
            res.end();
        }
        db.student.create({
            ime: ime,
            indeks: indeks
        }).then(rez => {
            return rez.student;
        }).catch(function (err) {
            res.write("create nije uspio sa greskom " + err);
            res.end();
            return 0;
        });
    }
});


app.post("/v2/predmet", (req,res) => {
    let tijeloZahtjeva = JSON.stringify(req.body);
    
    let redovi = tijeloZahtjeva.split("},{");
    
    for (var i = 0; i < redovi.length;i++)
    {
        var naziv = redovi[i];
               
        db.predmet.findOne({
            where: { 
                naziv: naziv
            }
        }).then(() => {
            res.write("Predmet već postoji u tabeli predmeta " + naziv);
            res.end();
        });

        db.predmet.create({
            naziv: naziv
        }).then(rez => {
            return rez.predmet;
        }).catch(function (err) {
            res.write("create nije uspio sa greskom " + err);
            res.end();
            return 0;
        });
    }
});


app.post("/v2/grupa", (req,res) => {
    let tijeloZahtjeva = JSON.stringify(req.body);
    
    let redovi = tijeloZahtjeva.split("},{");
    
    for (var i = 0; i < redovi.length;i++)
    {
        var naziv = redovi[i];
               
        db.grupa.findOne({
            where: { 
                naziv: naziv
            }
        }).then(() => {
            res.write("Grupa već postoji u tabeli grupa " + naziv);
            res.end();
        });

        db.grupa.create({
            naziv: naziv
        }).then(rez => {
            return rez.grupa;
        }).catch(function (err) {
            res.write("create nije uspio sa greskom " + err);
            res.end();
            return 0;
        });
    }
});

app.post("/v2/dan", (req,res) => {
    let tijeloZahtjeva = JSON.stringify(req.body);
    
    let redovi = tijeloZahtjeva.split("},{");
    
    for (var i = 0; i < redovi.length;i++)
    {
        var naziv = redovi[i];
               
        db.dan.findOne({
            where: { 
                naziv: naziv
            }
        }).then(() => {
            res.write("Dan već postoji u tabeli dana " + naziv);
            res.end();
        });

        db.dan.create({
            naziv: naziv
        }).then(rez => {
            return rez.dan;
        }).catch(function (err) {
            res.write("create nije uspio sa greskom " + err);
            res.end();
            return 0;
        });
    }
});

app.post("/v2/tip", (req,res) => {
    let tijeloZahtjeva = JSON.stringify(req.body);
    
    let redovi = tijeloZahtjeva.split("},{");
    
    for (var i = 0; i < redovi.length;i++)
    {
        var naziv = redovi[i];
               
        db.tip.findOne({
            where: { 
                naziv: naziv
            }
        }).then(() => {
            res.write("Tip već postoji u tabeli tipova aktivnosti " + naziv);
            res.end();
        });

        db.tip.create({
            naziv: naziv
        }).then(rez => {
            return rez.tip;
        }).catch(function (err) {
            res.write("create nije uspio sa greskom " + err);
            res.end();
            return 0;
        });
    }
});


app.post("/v2/aktivnost", (req,res) => {
    let tijeloZahtjeva = JSON.stringify(req.body);    
    let redovi = tijeloZahtjeva.split("},{");
    
    for (var i = 0; i < redovi.length;i++)
    {
        var elementi = redovi[i].split(",");
        var naziv = (elementi[0].split(":")[1]).replace(/\W/g, '');
        var pocetak = (elementi[1].split(":")[1]).replace(/\W/g, '');
        var kraj = (elementi[2].split(":")[1]).replace(/\W/g, '');
       
        db.aktivnost.findOne({
            where: { 
                naziv: naziv
            }
        }).then(() => {
            res.write("Aktivnost vec postoji u tabeli aktivnosti " + naziv);
            res.end();
        });
        
        db.aktivnost.create({
            naziv: naziv,
            pocetak: pocetak,
            kraj : kraj
        }).then(rez => {
            return rez.aktivnost;
        }).catch(function (err) {
            res.write("create nije uspio sa greskom " + err);
            res.end();
            return 0;
        });
    }
});

/**********************CREATE */

app.put("/v2/student", (req,res) => {
    var tijeloZahtjeva = req.body.studenti;

    tijeloZahtjeva.forEach(linija => {
        elementi = linija.split(",");
        db.student.update({
            ime: elementi[0],
            index: elementi[1]
        }, {where: {id: id}}).then((id) => {
            return id.student;
        }).catch(function (err) {
            console.log("neuspješan update, error: " + err);
            return 0;
        });
    });
});

app.put("/v2/aktivnost", (req,res) => {
    var tijeloZahtjeva = req.body.aktivnost;

    tijeloZahtjeva.forEach(linija => {
        elementi = linija.split(",");
        db.aktivnost.update({
            naziv: elementi[0],
            pocetak: elementi[1],
            kraj: elementi[2]
        }, {where: {id: id}}).then((id) => {
            return id.aktivnost;
        }).catch(function (err) {
            console.log("neuspješan update, error: " + err);
            return 0;
        });
    });
});


app.put("/v2/dan", (req,res) => {
    var tijeloZahtjeva = req.body.dan;

    tijeloZahtjeva.forEach(linija => {        
        db.aktivnost.update({
            naziv: linija
        }, {where: {id: id}}).then((id) => {
            return id.dan;
        }).catch(function (err) {
            console.log("neuspješan update, error: " + err);
            return 0;
        });
    });
});

app.put("/v2/predmet", (req,res) => {
    var tijeloZahtjeva = req.body.predmet;

    tijeloZahtjeva.forEach(linija => {        
        db.predmet.update({
            naziv: linija
        }, {where: {id: id}}).then((id) => {
            return id.predmet;
        }).catch(function (err) {
            console.log("neuspješan update, error: " + err);
            return 0;
        });
    });
});

app.put("/v2/grupa", (req,res) => {
    var tijeloZahtjeva = req.body.grupa;

    tijeloZahtjeva.forEach(linija => {        
        db.grupa.update({
            naziv: linija
        }, {where: {id: id}}).then((id) => {
            return id.grupa;
        }).catch(function (err) {
            console.log("neuspješan update, error: " + err);
            return 0;
        });
    });
});

app.put("/v2/tip", (req,res) => {
    var tijeloZahtjeva = req.body.tip;

    tijeloZahtjeva.forEach(linija => {        
        db.tip.update({
            naziv: linija
        }, {where: {id: id}}).then((id) => {
            return id.tip;
        }).catch(function (err) {
            console.log("neuspješan update, error: " + err);
            return 0;
        });
    });
});

/****************READ */
app.get("/v2/student", (req,res) => {
    db.student.findAll().then(student => {
        console.log(student);
    }).catch(function (err) {
        console.log("neuspješno čitanje, error: " + err);
        return 0;
    });
});

app.get("/v2/dan", (req,res) => {
    db.dan.findAll().then(dan => {
        console.log(dan);
    }).catch(function (err) {
        console.log("neuspješno čitanje, error: " + err);
        return 0;
    });
});

app.get("/v2/tip", (req,res) => {
    db.tip.findAll().then(tip => {
        console.log(tip);
    }).catch(function (err) {
        console.log("neuspješno čitanje, error: " + err);
        return 0;
    });
});

app.get("/v2/aktivnost", (req,res) => {
    db.aktivnost.findAll().then(aktivnost => {
        console.log(aktivnost);
    }).catch(function (err) {
        console.log("neuspješno čitanje, error: " + err);
        return 0;
    });
});

app.get("/v2/predmet", (req,res) => {
    db.predmet.findAll().then(predmet => {
        console.log(predmet);
    }).catch(function (err) {
        console.log("neuspješno čitanje, error: " + err);
        return 0;
    });
});

app.get("/v2/grupa", (req,res) => {
    db.grupa.findAll().then(grupa => {
        console.log(grupa);
    }).catch(function (err) {
        console.log("neuspješno čitanje, error: " + err);
        return 0;
    });
});


/************************DELETE */
app.delete("/v2/student", (req,res) => {
    db.student.deleteOne({ where: {id: id}
    }).then((id) => {
        console.log(id);
    }).catch(function (err) {
        console.log("greška u brisanju, error: " + err);
        return 0;
    });
});

app.delete("/v2/grupa", (req,res) => {
    db.grupa.deleteOne({ where: {id: id}
    }).then((id) => {
        console.log(id);
    }).catch(function (err) {
        console.log("greška u brisanju, error: " + err);
        return 0;
    });
});

app.delete("/v2/dan", (req,res) => {
    db.dan.deleteOne({ where: {id: id}
    }).then((id) => {
        console.log(id);
    }).catch(function (err) {
        console.log("greška u brisanju, error: " + err);
        return 0;
    });
});

app.delete("/v2/tip", (req,res) => {
    db.tip.deleteOne({ where: {id: id}
    }).then((id) => {
        console.log(id);
    }).catch(function (err) {
        console.log("greška u brisanju, error: " + err);
        return 0;
    });
});

app.delete("/v2/aktivnost", (req,res) => {
    db.aktivnost.deleteOne({ where: {id: id}
    }).then((id) => {
        console.log(id);
    }).catch(function (err) {
        console.log("greška u brisanju, error: " + err);
        return 0;
    });
});

app.delete("/v2/predmet", (req,res) => {
    db.predmet.deleteOne({ where: {id: id}
    }).then((id) => {
        console.log(id);
    }).catch(function (err) {
        console.log("greška u brisanju, error: " + err);
        return 0;
    });
});



app.listen(8080, () => {
    db.sequelize.sync({force: true})
});