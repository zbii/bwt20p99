//Funkcija koja uz pomoć regex izraza provjerava validnost vrijednosti vremena
function provjeriVrijeme(element) {
    return element.match(/^([0-1]?[0-9]|2[0-3]):[0-5][0-9]$/);
}

//Funkcija koja uz pomoć regex izraza provjerava da li je uneseno ispravno ime predmeta 
//U slučaju da je odabrana aktivnost predavanja (prihvata unos oblika MUR, MUR3)
function provjeriImeP(element) {
    return element.match(/^[A-Z]+[0-9]?$/);
}

//Funkcija koja uz pomoć regex izraza provjerava da li je uneseno ispravno ime predmeta 
//U slučaju da je odabrana aktivnost vježbe (prihvata unose oblika MUR-grupa1, MUR3-grupa1)
function provjeriImeV(element) {
    return element.match(/^[A-Z]+ ?[0-9]?-grupa ?[0-9]+$/);
}

//Pomoćna funkcija koja prihvata vrijeme u obliku stringa HH:MM, 
//uzme vrijednosti na određenim indeksima i pretvara ga u vrijednost vremena u minutama
function dajUMinutama(vrijeme) {
    return (parseInt(vrijeme[3] * 10) + parseInt(vrijeme[4]) + (parseInt(vrijeme[0] * 10) + parseInt(vrijeme[1])) * 60);
}

//Funkcija koja upoređuje početno i krajnje vrijeme
//Početno mora biti manje od krajnjeg
function uporediVremena(vrijeme1, vrijeme2) {
    return (dajUMinutama(vrijeme1) < dajUMinutama(vrijeme2));
}

function validirajVrijeme(vrijeme) {
    return !(vrijeme == "" || vrijeme.length != 5 || provjeriVrijeme(vrijeme) == null);
}

function validirajFormu() {

    /*Validacija podudarnosti*/
    //Ako je kao aktivnost uneseno predavanje, onda ime predmeta treba sadržavati samo inicijale predmeta i eventualno broj
    //Ako je kao aktivnost unesena vježba, onda ime predmeta osim inicijala predmeta treba sadržavati crticu i grupu
    //U te svrhe sam implementirala dodatne funkcije koje se pozivaju u zavisnosti od vrste aktivnosti,
    //i uz pomoć regexa provjeravaju ispravnost unosa

    var ime = document.forms["unos"]["naziv"].value;

    if (ime.length == 0) {
        alert("Molimo unesite ime predmeta");
        return false;
    }

    var aktivnost = document.forms["unos"]["aktivnost"].value;

    if (aktivnost == "predavanje") {
        if (provjeriImeP(ime) == null) {
            alert("Molimo unesite validno ime predmeta");
            return false;
        }
    } else if (aktivnost == "vjezbe") {
        if (provjeriImeV(ime) == null) {
            alert("Molimo unesite validno ime predmeta i grupu u formatu predmet-grupa");
            return false;
        }
    }

    /*Validacija vremena*/
    //Validacija unesenog vremena se provjerava korištenjem regex izraza na format HH:MM
    //Na kraju se provjerava da li je vrijeme početka aktivnosti prije vremena kraja aktivnosti

    var startHH = document.forms["unos"]["startHH"].value;
    var startMM = document.forms["unos"]["startMM"].value;
    var start = startHH + ':' + startMM;
    if (validirajVrijeme(startHH + ':' + startMM) == false) {
        alert("Molimo unesite validno početno vrijeme formata HH:MM!");
        return false;
    }

    var endHH = document.forms["unos"]["endHH"].value;
    var endMM = document.forms["unos"]["endMM"].value;
    var end = endHH + ':' + endMM;
    if (validirajVrijeme(end) == false) {
        alert("Molimo unesite validno krajnje vrijeme formata HH:MM!");
        return false;
    }

    if (uporediVremena(start, end) == false) {
        alert("Vrijeme početka vježbe mora biti prije vremena kraja!");
        return false;
    }

}