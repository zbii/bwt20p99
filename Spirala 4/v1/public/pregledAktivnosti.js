let dan = null;
let atribut = null;
let kako = 'A';

function generisiRedove(data, err) {

    var tabela = document.getElementById("tijeloTabele");
    document.getElementById('tijeloTabele').innerHTML = "";
       
    if (err) 
    {
            var red = tabela.insertRow(-1);
            var td1 = red.insertCell(-1);
            var td2 = red.insertCell(-1);
            var td3 = red.insertCell(-1);
            var td4 = red.insertCell(-1);
            var td5 = red.insertCell(-1);
            td1.innerHTML = "";
            td2.innerHTML = "";
            td3.innerHTML = "";
            td4.innerHTML = "";
            td5.innerHTML = "";
    }
    else
    {
        for (var i = 0; i < data.length; i++) 
        {
            var red = tabela.insertRow(-1);
            var td1 = red.insertCell(-1);
            var td2 = red.insertCell(-1);
            var td3 = red.insertCell(-1);
            var td4 = red.insertCell(-1);
            var td5 = red.insertCell(-1);
            td1.innerHTML = data[i].naziv;
            td2.innerHTML = data[i].aktivnost;
            td3.innerHTML = data[i].dan;
            td4.innerHTML = data[i].pocetak;
            td5.innerHTML = data[i].kraj;
        }    
    }        
}

function sortBy(poCemu) 
{
    if(dan == "") 
        dan = null;
     
    if (poCemu == atribut)
        {
            if (kako == 'D')
                kako = 'A';
            else kako = 'D';
        }
    else
        atribut = poCemu;        

    ucitajSortirano(dan, kako + atribut, generisiRedove);
}

var el = document.getElementById('dani');
if (el)
{
    el.addEventListener('change', (event) => {  
        if (el.value == "")
            ucitajSortirano(null, kako + atribut, generisiRedove);
        else ucitajSortirano(el.value, kako + atribut, generisiRedove);
        
       dan = event.target.value;
    });
    dan = el.value;
}

ucitajSortirano(null, null, generisiRedove);